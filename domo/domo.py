#!/usr/bin/env python
#-*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
import sys

LINES = {
  'on': 16,
  'off': 13,
  'relay1': 7,
  'relay2': 11,
  'relay3': 12,
  'relay4': 15,
}

PULSE = 1

print "init..."
GPIO.setmode(GPIO.BOARD)
for pin in LINES.values():
    GPIO.setup(pin, GPIO.OUT, initial=0)
print "init fini"

def turn(mode, num):
    GPIO.setmode(GPIO.BOARD)
    r_pin = LINES['relay%d' % num]
    m_pin = LINES[mode]
    GPIO.output(r_pin, 1)
    GPIO.output(m_pin, 1)
    time.sleep(PULSE)
    GPIO.output(r_pin, 0)
    GPIO.output(m_pin, 0)


if __name__ == '__main__':
    for arg in sys.argv[1:]:
        mode, num = arg.split(':', 1)
        turn(mode, int(num))
