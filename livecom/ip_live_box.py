#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json

BASE_URL = 'http://192.168.1.1'
LOGIN_DATA = 'username=admin&password=admin'

x = requests.post('%s/authenticate?%s' % (BASE_URL, LOGIN_DATA),
    data=LOGIN_DATA,
    headers={'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})
y = requests.post('%s/sysbus/NMC:getWANStatus' % BASE_URL,
    data='{"parameters":{}}',
    cookies=x.cookies,
    headers={'Content-Type': 'application/x-sah-ws-1-call+json; charset=UTF-8'})

d = json.loads(y.text)

print(d['result']['data']['IPAddress'])
