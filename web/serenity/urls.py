from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns_not_rooted = patterns('',
    # Examples:
    # url(r'^$', 'serenity.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('lights.urls', namespace="lights")),
)

urlpatterns = patterns('',
    url(r'^domo/', include(urlpatterns_not_rooted)),
)
