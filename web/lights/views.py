from django.shortcuts import render, redirect
import subprocess

BIN=['sudo', '-n', '/home/dstan/serenIT/domo/domo.py']
# Create your views here.

AVAILABLE = [1,2,3,4]

_enabled = { x: False for x in AVAILABLE}

def boutons(request):
    buttons = list()
    for i in AVAILABLE:
        buttons.append({'label': u'%d' % i, 'on': _enabled[i]})
    return render(request, 'index.html', {'buttons': buttons})

def enable(request):
    try:
        num = int(request.POST['num'])
    except Exception:
        num = None
    if num not in AVAILABLE:
        num = AVAILABLE[0]
    _enabled[num] = not _enabled[num]
    arg = '%s:%d' % (('on' if _enabled[num] else 'off'), num)
    subprocess.check_output(BIN + [arg])
    return redirect('lights:boutons')
    
